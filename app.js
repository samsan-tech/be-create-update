const express = require("express");
const bodyParser = require("body-parser");
var cors = require("cors");
const { db, redisClient } = require("./db");
const { response } = require("express");
// Constants
const PORT = 8080;

// App
const app = express();
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post("/create-user", (req, res) => {
  var client = db();
  var redis = redisClient();
  client.connect((err) => {
    const collection = client.db("tkai").collection("Tkai");
    // perform actions on the collection object
    let name = req.body.name;
    let age = parseInt(req.body.age);
    let weight = parseFloat(req.body.weight);
    let height = parseFloat(req.body.height);
    let bmi = calculateBmi(weight, height);
    let need_update = false;
    let update_date = req.body.update_bmi;
    let update_time = parseInt(new Date(update_date).getTime() / 1000).toFixed(
      0
    );
    let dateNow = parseInt(new Date().getTime() / 1000).toFixed(0);

    // redis.set(name, update_time)
    if (!isNaN(update_time)) {
      console.log(dateNow)
      console.log(update_time)

      if (dateNow >= update_time) {
        need_update = true;
      }
      redis.set(name, update_time.toString());
    }
    let user = {
      name: name,
      age: age,
      weight: weight,
      height: height,
      bmi: bmi,
      need_update: need_update,
    };

    collection.insertOne(user, (err, result) => {
      if (err) {
        throw err;
      }
      console.log("Inserted");
      res.status(200).json({
        message: "User inserted"
      });
    });
    client.close();
  });


});

app.post("/update-user", (req, res) => {
  var client = db();
  var redis = redisClient();
  client.connect((err) => {
    const collection = client.db("tkai").collection("Tkai");
    // perform actions on the collection object
    let name = req.body.name;
    let age = parseInt(req.body.age);
    let weight = parseFloat(req.body.weight);
    let height = parseFloat(req.body.height);
    let bmi = calculateBmi(weight, height);
    let need_update = false;
    let update_date = req.body.update_bmi;
    let update_time = parseInt(new Date(update_date).getTime() / 1000).toFixed(
      0
    );
    redis.del(`${name}`)

    const filter = { name: req.body.name };
    const options = { upsert: true };
    const updateDoc = {
      $set: {
        name: name,
        age: age,
        weight: weight,
        height: height,
        bmi: bmi,
        need_update: need_update,
      },
    };

    collection.updateOne(filter, updateDoc, options, (err, result) => {
      if (err) throw err;
      console.log("Updated");
      res.status(200).json({
        message: "User Updated"
      });
    });
    client.close();
  });

});

function calculateBmi(weight, height) {
  return Math.round((weight / Math.pow(height, 2)) * 10000);
}

app.get('/', (req, res) => {
  const redis = redisClient();
  redis.set("udin-petot", "23914197")
  res.status(200).json({
    message: "inserted"
  })
})

app.get('/get-key', (req, res) => {
  const redis = redisClient();
  redis.get("udin-petot", (err, cb) => {
    console.log(cb)
    res.status(200).json({
      message: cb
    })
  })
})

app.listen(PORT);
console.log("Server listening on port: " + PORT);
