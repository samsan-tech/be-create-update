const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const redis = require('redis')
const db_user = process.env.DB_USERNAME;
const db_pass = process.env.DB_PASSWORD;
const redis_auth = process.env.REDIS_AUTH;

exports.db = function db() {
    // Connection URL
    const uri = `mongodb://${db_user}:${db_pass}@34.70.144.224:27016`;

    var client = new MongoClient(uri, { useNewUrlParser: true });
    return client
}

exports.redisClient = function redisClient() {
    let redis_url = "redis://34.70.144.224:6379"
    const clientRedis = redis.createClient(redis_url)
    clientRedis.auth(`${redis_auth}`)
    return clientRedis
}
